package ru.semenov.service;

import ru.semenov.model.TransferBalance;

import java.math.BigDecimal;

public interface IBankService {
    BigDecimal getBalance(Long accountId);

    BigDecimal addBalance(Long to, BigDecimal amount);

    void transferBalance(TransferBalance transferBalance);
}
