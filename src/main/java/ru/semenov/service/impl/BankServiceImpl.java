package ru.semenov.service.impl;

import org.springframework.stereotype.Service;
import ru.semenov.model.TransferBalance;
import ru.semenov.repository.BalanceRepository;
import ru.semenov.service.IBankService;

import java.math.BigDecimal;

@Service
public class BankServiceImpl implements IBankService {
    private final BalanceRepository balanceRepository;

    public BankServiceImpl(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    @Override
    public BigDecimal getBalance(Long accountId) {
        BigDecimal balanceById = balanceRepository.getBalanceById(accountId);
        if(balanceById==null){
            throw new IllegalArgumentException();
        }
        return balanceById;
    }

    @Override
    public BigDecimal addBalance(Long to, BigDecimal amount) {
        BigDecimal currentBalance = balanceRepository.getBalanceById(to);
        if(currentBalance==null){
            balanceRepository.save(to,amount);
            return amount;
        }else {
            BigDecimal updatedBalance = currentBalance.add(amount);
            balanceRepository.save(to,updatedBalance);
            return updatedBalance;
        }
    }

    @Override
    public void transferBalance(TransferBalance transferBalance) {
        BigDecimal fromBalance = balanceRepository.getBalanceById(transferBalance.getFrom());
        BigDecimal toBalance = balanceRepository.getBalanceById(transferBalance.getTo());
        if(fromBalance==null|| toBalance == null) {
            throw new IllegalArgumentException();
        }
        if(transferBalance.getAmount().compareTo(fromBalance)>0){
            throw new IllegalArgumentException("Недостаточно средств!!!");
        }
        BigDecimal updatedFromBalance = fromBalance.subtract(transferBalance.getAmount());
        BigDecimal updatedToBalance = toBalance.add(transferBalance.getAmount());
        balanceRepository.save(transferBalance.getFrom(), updatedFromBalance);
        balanceRepository.save(transferBalance.getTo(), updatedToBalance);
    }
}
