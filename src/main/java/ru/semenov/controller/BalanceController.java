package ru.semenov.controller;

import org.springframework.web.bind.annotation.*;
import ru.semenov.model.TransferBalance;
import ru.semenov.service.IBankService;

import java.math.BigDecimal;

@RestController("/balance")
public class BalanceController {
    private final IBankService bankService;

    public BalanceController(IBankService bankService) {
        this.bankService = bankService;
    }

    @GetMapping("/{accountId}")
    public BigDecimal getBalance(@PathVariable Long accountId) {
        return bankService.getBalance(accountId);
    }
    @PostMapping("/add")
    public BigDecimal addBalance(@RequestBody TransferBalance transferBalance) {
        return bankService.addBalance(transferBalance.getTo(),transferBalance.getAmount());
    }

    @PostMapping("/transfer")
    public void transferBalance(@RequestBody TransferBalance transferBalance) {
        bankService.transferBalance(transferBalance);
    }

}
