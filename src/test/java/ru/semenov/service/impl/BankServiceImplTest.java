package ru.semenov.service.impl;

import org.junit.jupiter.api.Test;
import ru.semenov.repository.BalanceRepository;

import java.math.BigDecimal;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankServiceImplTest {
    private BalanceRepository balanceRepository = new BalanceRepository();
    private BankServiceImpl bankService = new BankServiceImpl(balanceRepository);

    @Test
    void addMoney(){
        bankService.addBalance(1L,BigDecimal.TEN);
        BigDecimal balanceById = balanceRepository.getBalanceById(1L);
        assertEquals(balanceById, BigDecimal.TEN);
    }
}
