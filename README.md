# Приложение для просмотра и управления собственным счетом

Данное приложение разработано на Java 17 с использованием следующих технологий: Spring 3, Swagger, Maven, Spring Web и Lombok. Оно предоставляет возможность просмотра собственного счета, добавления денег на свой счет и перевода денег с одного счета на другой.

## Технологии
Приложение разработано на языке Java с использованием следующих технологий и инструментов:

- Java 17
- Spring Framework 3
- Swagger
- Maven
- Spring Web
- Lombok

## Зависимости
Перед сборкой и запуском приложения, убедитесь, что следующие зависимости установлены:

- Java 17
- Maven

## Установка и запуск
1. Склонируйте репозиторий приложения на свой локальный компьютер.
2. Откройте проект в вашей среде разработки (например, IntelliJ IDEA).
3. Установите все зависимости, выполнив команду `mvn install` в корневой директории проекта.
4. Запустите приложение, выполните команду `mvn spring-boot:run`.
5. Приложение будет запущено на порту 8080.

## Классы

### TransferBalance

```java
@Data
public class TransferBalance {
    private Long from;
    private Long to;
    private BigDecimal amount;
}
```

Класс `TransferBalance` представляет информацию о переводе денег. Он содержит поля `from` (счет, с которого будет производиться перевод), `to` (счет, на который будет производиться перевод) и `amount` (сумма перевода).

### BalanceRepository

```java
@Repository
public class BalanceRepository {
    private final Map<Long, BigDecimal> storage = new HashMap<Long, BigDecimal>();

    public BigDecimal getBalanceById(Long accountId) {
        return storage.get(accountId);
    }

    public void save(Long id, BigDecimal amount) {
        storage.put(id, amount);
    }
}
```

Класс `BalanceRepository` представляет репозиторий для работы с данными о счетах и их балансах. Он использует `Map` для хранения пар ключ-значение, где ключом является идентификатор счета, а значением - текущий баланс.

### BankServiceImpl

```java
@Service
public class BankServiceImpl implements IBankService {
    private final BalanceRepository balanceRepository;

    public BankServiceImpl(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    @Override
    public BigDecimal getBalance(Long accountId) {
        BigDecimal balanceById = balanceRepository.getBalanceById(accountId);
        if(balanceById==null){
            throw new IllegalArgumentException();
        }
        return balanceById;
    }

    @Override
    public BigDecimal addBalance(Long to, BigDecimal amount) {
        BigDecimal currentBalance = balanceRepository.getBalanceById(to);
        if(currentBalance==null){
            balanceRepository.save(to,amount);
            return amount;
        }else {
            BigDecimal updatedBalance = currentBalance.add(amount);
            balanceRepository.save(to,updatedBalance);
            return updatedBalance;
        }
    }

    @Override
    public void transferBalance(TransferBalance transferBalance) {
        BigDecimal fromBalance = balanceRepository.getBalanceById(transferBalance.getFrom());
        BigDecimal toBalance = balanceRepository.getBalanceById(transferBalance.getTo());
        if(fromBalance==null|| toBalance == null) {
            throw new IllegalArgumentException();
        }
        if(transferBalance.getAmount().compareTo(fromBalance)>0){
            throw new IllegalArgumentException("Недостаточно средств!!!");
        }
        BigDecimal updatedFromBalance = fromBalance.subtract(transferBalance.getAmount());
        BigDecimal updatedToBalance = toBalance.add(transferBalance.getAmount());
        balanceRepository.save(transferBalance.getFrom(), updatedFromBalance);
        balanceRepository.save(transferBalance.getTo(), updatedToBalance);
    }
}
```

Класс `BankServiceImpl` содержит бизнес-логику приложения. Он реализует интерфейс `IBankService` и использует `BalanceRepository` для работы с данными о счетах и их балансах. В нем реализованы методы для получения баланса, добавления денег на счет и перевода денег.

### BalanceController

```java
@RestController("/balance")
public class BalanceController {
    private final IBankService bankService;

    public BalanceController(IBankService bankService) {
        this.bankService = bankService;
    }

    @GetMapping("/{accountId}")
    public BigDecimal getBalance(@PathVariable Long accountId) {
        return bankService.getBalance(accountId);
    }

    @PostMapping("/add")
    public BigDecimal addBalance(@RequestBody TransferBalance transferBalance) {
        return bankService.addBalance(transferBalance.getTo(), transferBalance.getAmount());
    }

    @PostMapping("/transfer")
    public void transferBalance(@RequestBody TransferBalance transferBalance) {
        bankService.transferBalance(transferBalance);
    }
}
```